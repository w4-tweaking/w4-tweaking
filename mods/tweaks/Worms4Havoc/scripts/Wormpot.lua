-------------------------------------------------------------------------------
-- Wormpot.lua
-- Manages most WormPot settings
-------------------------------------------------------------------------------

-- Iterator functions that make everything easier

GetAllWorms=function()
	return function(s, var)
		local w = lib_GetWormContainerName(var)
		if w=="" then return nil end
		return var+1, w
	end, 0, 0
end

GetAllTeamInventories=function()
	return function(s, var)
		local i = lib_GetTeamInventoryName(var)
		if i=="" then return nil end
		return var+1, i
	end, 0, 0
end

GetAllAllianceInventories=function()
	return function(s, var)
		local i = lib_GetAllianceInventoryName(var)
		if i=="" then return nil end
		return var+1, i
	end, 0, 0
end

GetAllWormInventories=function()
	return function(s, var)
		local i = lib_GetWormInventoryName(var)
		if i=="" then return nil end
		return var+1, i
	end, 0, 0
end

-- Critical chances for every weapon (from 0 to 100)
-- Can have a Related key with a list of every sub-weapon related to that weapon (clusters)
-- and a Trail key with the critical particle trail associated with that weapon

WeaponCriticalChance = {
	kWeaponBazooka={30},
	kWeaponGrenade={30},
	kWeaponClusterGrenade={30},
	kWeaponAirstrike={15, Related={kWeaponAirstrike2={}, kWeaponAirstrike3={}, kWeaponAirstrike4={}, kWeaponAirstrike5={}}},
	kWeaponSheep={20},
	kWeaponHomingMissile={15},
}

-- Dunno what this is for D:
function EndEarthquake()
	SendMessage("Earthquake.End")
end

-- Quick function for getting the alliance ID for a given team
function GetAllianceIdx(TeamIdx)
	local team = QueryContainer(lib_GetTeamContainerName(TeamIdx))
	return team.AlliedGroup
end

function DoWormpotOncePerTurnFunctions()
	-- Any WormPot setting that applies after every turn goes gere
	local Wormpot = QueryContainer("WormPot")
	
	-- Crates Only / Crate Shower
	if Wormpot.CratesOnly or Wormpot.LotsOfCrates then
		SendMessage("GameLogic.CrateShower")
	end

    -- Double damage
	if Wormpot.DoubleDamage then
		SetData("DoubleDamage",1)
	end
	
	-- Energy or enemy
	if Wormpot.EnergyOrEnemy then
		local PoisonRate = GetData("Worm.Poison.Default")
		-- Poison each worm
		for WormIndex,WormContainerName in GetAllWorms() do
			local lock, worm = EditContainer(WormContainerName)
			worm.PoisonRate = PoisonRate
			CloseContainer(lock)
			
			SendIntMessage("Worm.Poison",WormIndex);
		end
	end
	
	-- Replace this with Justice mode
	if Wormpot.DavidAndGoliath then
		if not TeamCycleState then TeamCycleState = {} end
		if TeamCycleState[LastActiveTeam] then -- Have all teams already played their turn?
			TeamCycleState = {}
			ApplyScalesOfJustice()
		end
		TeamCycleState[LastActiveTeam] = true
	end
end

function DoWormpotTurnStartFunctions()
	-- Any WormPot setting that applies before every turn goes gere
	local Wormpot = QueryContainer("WormPot")
	
	-- Replace this with Critical Hits mode
	if Wormpot.NoParachuteDrops then
		ResetCriticals()
		-- Calculate the total health for each alliance
		local AllianceTotalHealth = {0,0,0,0}
		local NumOfTeams = 0
		
		for _,WormContainerName in GetAllWorms() do
			local worm = QueryContainer(WormContainerName)
		
			if worm.Active and worm.Energy>0 then
				local team = GetAllianceIdx(worm.TeamIndex) + 1
				if AllianceTotalHealth[team]==0 then NumOfTeams = NumOfTeams + 1 end
				AllianceTotalHealth[team] = AllianceTotalHealth[team] + worm.Energy
			end
		end
		
		local CurrentTeam = 1+GetAllianceIdx(lib_GetWormTeamIndex(GetData("ActiveWormIndex")))
		local Average = (AllianceTotalHealth[1] + AllianceTotalHealth[2] + AllianceTotalHealth[3] + AllianceTotalHealth[4]) / NumOfTeams
		local CritMultiplier = 2 - AllianceTotalHealth[CurrentTeam]/Average
		-- Alliances that have a total health lower than the average will have a higher critical chance
		-- while alliances that have a total health higher than 2x the average will have no criticals at all
		
		for weapon,data in pairs(WeaponCriticalChance) do
			if lib_GetRandomFloat(1, 100)<CritMultiplier*data[1] then
				SetWeaponCritical(weapon, CurrentTeam, data)
			end
		end
	end
	
	-- Replace this with Justice mode
	if Wormpot.DavidAndGoliath then
		if not TeamCycleState then TeamCycleState = {} end
		local worm = GetData("ActiveWormIndex")
		if worm~="" then LastActiveTeam = GetAllianceIdx(lib_GetWormTeamIndex(worm)) end
	end
end

-----------------------------------------------------------------------------

function SetWormpotModes()
	-- This is executed when the game starts
    SendMessage("Wormpot.SetupModes")
    
	local WormpotLock, Wormpot 
	local ContainerLock, Container
	local DamageScale, PowerScale
	
	WaitingForWormpot = false
	
	local Wormpot = QueryContainer("WormPot")
	
	DamageScale = Wormpot.SuperScale
	PowerScale = Wormpot.PowerScale

	-- HEY TEAM 17, WHEN YOU DO AN IF STATEMENT ON A BOOLEAN VARIABLE, YOU DON'T NEED TO ADD ==true
	-- LOLOLOLOLO
	
	-- Dim Mak - Replaced by Kamikaze mode
	if Wormpot.DeathTouch then
		-- Lots of damage when worms die
		SetData("Worm.DeathImpulseMagnitude"   , 2)
		SetData("Worm.DeathImpulseRadius"      , 150)
		SetData("Worm.DeathLandDamageRadius"   , 110)
		SetData("Worm.DeathWormDamageMagnitude", 150)
		SetData("Worm.DeathWormDamageRadius"   , 120)
		
		-- Enable large worm explosion effect
		local ContainerLock, Container = EditContainer("WXPA_ExplosionLargeStarter")
		Container.EmitterMaxParticles = 1
		CloseContainer(ContainerLock)
		
		-- Cancel Dim-Mak effects
		ContainerLock, Container = EditContainer("kWeaponProd")
		Container.InstantKill = false
		Container.WormCollisionFX = ""
		CloseContainer(ContainerLock)
	end
	
	-- No bombing - Replaced by Critical Hits mode
	if Wormpot.NoParachuteDrops then
		ResetCriticals()
		
		-- TODO : Cancel no parachute drop effects
	end

	-- David and goliath - Replaced by Justice mode
	if Wormpot.DavidAndGoliath then
		ApplyScalesOfJustice() -- Apply once when the game starts, in case every team doesn't have the same number of worms
	end
	
	
	-- Tug o' Worms - Replaced by Weapons Don't End Turn mode
	if Wormpot.RopingArtilleryWorms then
	    -- No retreat time
	    SetData("DefaultRetreatTime",0)
  		SetData("RetreatTime",0)
		ContainerLock, Container = EditContainer("kWeaponDynamite")
		Container.RetreatTimeOverride = -1
		CloseContainer(ContainerLock)
		ContainerLock, Container = EditContainer("kWeaponLandmine")
		Container.RetreatTimeOverride = -1
		CloseContainer(ContainerLock)
		ContainerLock, Container = EditContainer("kWeaponSheep")
		Container.RetreatTimeOverride = -1
		CloseContainer(ContainerLock)
		
		-- Cancel artillery mode effects
		local Scheme = QueryContainer("GM.SchemeData")
		for WormIndex=0,15 do
	        ContainerLock, Container = EditContainer(lib_GetWormContainerName(WormIndex))
		    --if Scheme.ArtileryMode~=1 then
			    worm.ArtilleryMode = false
		    --end
			CloseContainer(ContainerLock)
        end
	end
	
	-- Super Firearms
	if Wormpot.SuperFirearms then
		ApplyWormpotDamageScale("kWeaponShotgun"    ,DamageScale)
		ApplyWormpotDamageScale("kWeaponSniperRifle",DamageScale)

		ApplyWormpotPowerScale("kWeaponShotgun"    ,PowerScale)
		ApplyWormpotPowerScale("kWeaponSniperRifle",PowerScale)
	end
	
	-- Super hand to hand weapons
	if Wormpot.SuperHand then
		ApplyWormpotDamageScale("kWeaponBaseballBat",DamageScale)
		ApplyWormpotDamageScale("kWeaponProd"       ,DamageScale)
		ApplyWormpotDamageScale("kWeaponFirePunch"  ,DamageScale)
		ApplyWormpotDamageScale("kWeaponNoMoreNails",DamageScale)

		ApplyWormpotPowerScale("kWeaponBaseballBat",PowerScale)
		ApplyWormpotPowerScale("kWeaponProd"       ,PowerScale)
		ApplyWormpotPowerScale("kWeaponFirePunch"  ,PowerScale)
		ApplyWormpotPowerScale("kWeaponNoMoreNails",PowerScale)
	end
	
	-- Super cluster weapons
	if Wormpot.SuperClusters then
		ApplyWormpotDamageScale("kWeaponClusterGrenade",DamageScale)
		ApplyWormpotDamageScale("kWeaponClusterBomb"   ,DamageScale)
		ApplyWormpotDamageScale("kWeaponAirstrike"     ,DamageScale)
		ApplyWormpotDamageScale("kWeaponSuperAirstrike",DamageScale)
		ApplyWormpotDamageScale("kWeaponBananaBomb"    ,DamageScale)
		ApplyWormpotDamageScale("kWeaponBananette"     ,DamageScale)

		ApplyWormpotPowerScale("kWeaponClusterGrenade",PowerScale)
		ApplyWormpotPowerScale("kWeaponClusterBomb"   ,PowerScale)
		ApplyWormpotPowerScale("kWeaponAirstrike"     ,PowerScale)
		ApplyWormpotPowerScale("kWeaponSuperAirstrike",PowerScale)
		ApplyWormpotPowerScale("kWeaponBananaBomb"    ,PowerScale)
		ApplyWormpotPowerScale("kWeaponBananette"     ,PowerScale)
	end
	
	-- Super animal weapons
	if Wormpot.SuperAnimals then
		ApplyWormpotDamageScale("kWeaponSheep",DamageScale)
		ApplyWormpotDamageScale("kWeaponSuperSheep",DamageScale)
		ApplyWormpotDamageScale("kWeaponOldWoman",DamageScale)
		ApplyWormpotDamageScale("kWeaponConcreteDonkey",DamageScale)
		ApplyWormpotDamageScale("kWeaponScouser",DamageScale)

		ApplyWormpotPowerScale("kWeaponSheep",PowerScale)
		ApplyWormpotPowerScale("kWeaponSuperSheep",PowerScale)
		ApplyWormpotPowerScale("kWeaponOldWoman",PowerScale)
		ApplyWormpotPowerScale("kWeaponConcreteDonkey",PowerScale)
		ApplyWormpotPowerScale("kWeaponScouser",PowerScale)
	end

	-- Super explosive weapons
	if Wormpot.SuperExplosives then
		ApplyWormpotDamageScale("kWeaponBazooka",DamageScale)
		ApplyWormpotDamageScale("kWeaponDynamite",DamageScale)
		ApplyWormpotDamageScale("kWeaponGrenade",DamageScale)
		ApplyWormpotDamageScale("kWeaponHolyHandGrenade",DamageScale)
		ApplyWormpotDamageScale("kWeaponLandmine",DamageScale)
		ApplyWormpotDamageScale("kWeaponHomingMissile",DamageScale)
		ApplyWormpotDamageScale("kWeaponGasCanister",DamageScale)
		ApplyWormpotDamageScale("kWeaponFatkins",DamageScale)

		ApplyWormpotPowerScale("kWeaponBazooka",PowerScale)
		ApplyWormpotPowerScale("kWeaponDynamite",PowerScale)
		ApplyWormpotPowerScale("kWeaponGrenade",PowerScale)
		ApplyWormpotPowerScale("kWeaponHolyHandGrenade",PowerScale)
		ApplyWormpotPowerScale("kWeaponLandmine",PowerScale)
		ApplyWormpotPowerScale("kWeaponHomingMissile",PowerScale)
		ApplyWormpotPowerScale("kWeaponGasCanister",PowerScale)
		ApplyWormpotPowerScale("kWeaponFatkins",PowerScale)
	end
	
    -- One shot one kill (i.e set health to 1!)
	if  Wormpot.OneShotOneKill then
		for _,WormContainerName in GetAllWorms() do
			local lock, worm = EditContainer(WormContainerName)
			worm.Energy = 1
			CloseContainer(lock)
		end
	end
	
	if Wormpot.CratesOnly then
		SendMessage("GameLogic.ClearInventories")
		SendMessage("GameLogic.CrateShower")
	end

	if Wormpot.LotsOfCrates then
		SendMessage("GameLogic.CrateShower")
	end

    -- Double Damage
	if Wormpot.DoubleDamage then
		SetData("DoubleDamage",1)
	end

	-- Falling really hurts
	if Wormpot.FallingHurtsMore then
		fDamage = GetData("Worm.FallDamageRatio")
		fDamage = fDamage * Wormpot.FallingScale
		SetData("Worm.FallDamageRatio",fDamage)
	end

  	-- No retreat time
  	if Wormpot.NoRetreatTime then 
        -- Set the retreat time to zero
  		SetData("DefaultRetreatTime",0)
  		SetData("RetreatTime",0)
  		
  		-- Now remove the surrender from the inventories

  		-- Team inventory	
  		for _,InventoryName in GetAllTeamInventories() do
  			local Lock,Container = EditContainer(InventoryName)
  			Container.Surrender = 0
  			CloseContainer(Lock)
  		end
  		
  		-- Alliance inventory
  		for _,InventoryName in GetAllAllianceInventories() do
  			local Lock,Container = EditContainer(InventoryName)
  			Container.Surrender = 0
  			CloseContainer(Lock)
  		end
  		
  		-- Worms
  		for _,InventoryName in GetAllWormInventories() do
  			local Lock,Container = EditContainer(InventoryName)
  			Container.Surrender = 0
  			CloseContainer(Lock)
  		end
  	end
	
	-- Energy or enemy
	if Wormpot.EnergyOrEnemy then
		local PoisonRate = GetData("Worm.Poison.Default")
		-- Poison each worm
		for WormIndex,WormContainerName in GetAllWorms() do
			local lock, worm = EditContainer(WormContainerName)
			worm.PoisonRate = PoisonRate
			CloseContainer(lock)
			
			SendIntMessage("Worm.Poison",WormIndex);
		end
	end

	-- Slippy mode
	if Wormpot.SlippyMode then
      SetStickySlippyMode(Wormpot.SlippyModeScale)
	end

	-- Sticky mode
	if Wormpot.StickyMode then
	-- Note - Sticky mode scale is a value between 0 & 1. Multiplying it by 20
	-- gives a 'better' result here ie is an arbitrary value that works ok!
      SetStickySlippyMode(Wormpot.StickyModeScale * 20)
	end

	-- Wind effects more weapons
	if Wormpot.WindEffectMore then
		SetWeaponWind("kWeaponAirstrike",true)
		SetWeaponWind("kWeaponBananaBomb",true)
		SetWeaponWind("kWeaponBananette",true)
		SetWeaponWind("kWeaponClusterBomb",true)
		SetWeaponWind("kWeaponClusterGrenade",true)
		SetWeaponWind("kWeaponConcreteDonkey",true)
		SetWeaponWind("kWeaponGasCanister",true)
		SetWeaponWind("kWeaponGrenade",true)
		SetWeaponWind("kWeaponHolyHandGrenade",true)
		SetWeaponWind("kWeaponOldWoman",true)
		SetWeaponWind("kWeaponSheep",true)
		SetWeaponWind("kWeaponScouser",true)
		SetWeaponWind("kWeaponFatkins",true)
		SetWeaponWind("kWeaponPoisonArrow",true)
		SetWeaponWind("kWeaponFactoryWeapon",true)
		SetWeaponWind("kWeaponDynamite",true)
		SetWeaponWind("kWeaponLandmine",true)
		SetWeaponWind("kWeaponSuperAirstrike",true)
	end
	
	-- Double Health Crates
	-- Replaced by Super Rope
	if Wormpot.DoubleHealthCrates then
		SetData("Ninja.NumShots", -1)
		SetData("Ninja.MaxLength", 1800)
		SetData("Ninja.LengthenShortenRate", 0.3)
	end
end

------------------------------------------------------------------------
function ApplyWormpotDamageScale(ContainerName, Scale)
	local ContainerLock, Container = EditContainer(ContainerName)
	Container.WormDamageMagnitude = Container.WormDamageMagnitude * Scale
	Container.LandDamageRadius = Container.LandDamageRadius * Scale
	CloseContainer(ContainerLock)
end
	
------------------------------------------------------------------------
function ApplyWormpotParticleDamageScale(ContainerName, Scale)
	local ContainerLock, Container = EditContainer(ContainerName)
	Container.ParticleCollisionWormDamageMagnitude = Container.ParticleCollisionWormDamageMagnitude * Scale
	CloseContainer(ContainerLock)
end

------------------------------------------------------------------------
function ApplyWormpotPowerScale(ContainerName, Scale)
	local ContainerLock, Container = EditContainer(ContainerName)
	Container.ImpulseMagnitude = Container.ImpulseMagnitude * Scale
	CloseContainer(ContainerLock)
end

------------------------------------------------------------------------
function ApplyWormpotParticlePowerScale(ContainerName, Scale)
	local ContainerLock, Container = EditContainer(ContainerName)
	Container.ParticleCollisionWormImpulseMagnitude = Container.ParticleCollisionWormImpulseMagnitude * Scale
	Container.ParticleCollisionWormImpulseYMagnitude = Container.ParticleCollisionWormImpulseYMagnitude * Scale
	CloseContainer(ContainerLock)
end

------------------------------------------------------------------------
function SetWeaponWind(ContainerName, IsAffectedByWind)
	local ContainerLock, Container = EditContainer(ContainerName)
	Container.IsAffectedByWind = IsAffectedByWind
	CloseContainer(ContainerLock)
end

------------------------------------------------------------------------
function SetParticleWind(ContainerName, IsAffectedByWind)
	local ContainerLock, Container = EditContainer(ContainerName)
	Container.ParticleIsEffectedByWind = IsAffectedByWind
	CloseContainer(ContainerLock)
end


function SetStickySlippyMode(Scale)
   local f = GetData("Worm.SlideStopVel")
	f = f * Scale
	SetData("Worm.SlideStopVel",f)
		
	f = GetData("Worm.BounceMultiplier")
	f = f * ( 1.0 / Scale)
	SetData("Worm.BounceMultiplier",f)


   local muliplier = (1.0 / Scale)
   ApplyWormpotStickyScale("kWeaponBananaBomb", muliplier)
   ApplyWormpotStickyScale("kWeaponClusterGrenade", muliplier)
   ApplyWormpotStickyScale("kWeaponDynamite", muliplier)
   ApplyWormpotStickyScale("kWeaponGasCanister", muliplier)
   ApplyWormpotStickyScale("kWeaponGrenade", muliplier)
   ApplyWormpotStickyScale("kWeaponHolyHandGrenade", muliplier)
end


function ApplyWormpotStickyScale(ContainerName, Scale)
   local ContainerLock, Container = EditContainer(ContainerName)
	local NewParallelMinBounceDamping = Container.ParallelMinBounceDamping * Scale
   local NewTangentialMinBounceDamping = Container.TangentialMinBounceDamping * Scale
   if NewParallelMinBounceDamping > 1 then
      NewParallelMinBounceDamping = 1
   end   
   if NewTangentialMinBounceDamping > 1 then
      NewTangentialMinBounceDamping = 1
   end
   Container.ParallelMinBounceDamping = NewParallelMinBounceDamping
   Container.TangentialMinBounceDamping = NewTangentialMinBounceDamping
	CloseContainer(ContainerLock)
end 

------------------------------------------------------------------------
function DisallowAllWeapons(WormContainer)

    WormContainer.AllowBazooka = 0;             
    WormContainer.AllowGrenade = 0;            
    WormContainer.AllowClusterGrenade = 0;      
    WormContainer.AllowAirstrike = 0;           
    WormContainer.AllowDynamite = 0;            
    WormContainer.AllowHolyHandGrenade = 0;     
    WormContainer.AllowBananaBomb = 0;          
    WormContainer.AllowLandmine = 0;            
    WormContainer.AllowShotgun = 0;             
    WormContainer.AllowBaseballBat = 0;         
    WormContainer.AllowProd = 0;                
    WormContainer.AllowFirePunch = 0;           
    WormContainer.AllowHomingMissile = 0;       
    WormContainer.AllowSheep = 0;               
    WormContainer.AllowGasCanister = 0;         
    WormContainer.AllowOldWoman = 0;            
    WormContainer.AllowConcreteDonkey = 0;      
    WormContainer.AllowSuperSheep = 0;          
    WormContainer.AllowGirder = 0;             
    -- WormContainer.AllowBridgeKit = 0;          
    WormContainer.AllowNinjaRope = 0;          
    WormContainer.AllowParachute = 0;          
    WormContainer.AllowLowGravity = 0;         
    -- WormContainer.AllowTeleport = 0;           
    WormContainer.AllowJetpack = 0;            
    WormContainer.AllowSkipGo = 1;             
    WormContainer.AllowSurrender = 1;          
    WormContainer.AllowChangeWorm = 0;         
    WormContainer.AllowRedbull = 0;          
    WormContainer.AllowArmour = 0;   
    WormContainer.AllowStarburst = 0;
    WormContainer.AllowWeaponFactoryWeapon = 0;
    WormContainer.AllowAlienAbduction = 0;
    WormContainer.AllowFatkins = 0;
    WormContainer.AllowScouser = 0;
    WormContainer.AllowNoMoreNails = 0;
    -- WormContainer.AllowPipe = 0;
    WormContainer.AllowPoisonArrow = 0;
    WormContainer.AllowSentryGun = 0;
    WormContainer.AllowSniperRifle = 0;
    WormContainer.AllowSuperAirstrike = 0;
    WormContainer.AllowBubbleTrouble = 0;
    WormContainer.AllowFlood = 0;  
    
end


------------------------------------------------------------------------

function ApplyScalesOfJustice()
	local TotalHealth = 0
	local NumOfTeams = 0
	local TotalTeamHealth = {0,0,0,0}
	local NumOfWorms = {0,0,0,0}
	local HealthChanged = false
	
	-- Calculate the total health in each team first
	for _,WormContainerName in GetAllWorms() do
		local worm = QueryContainer(WormContainerName)
		
		if worm.Active and worm.Energy>0 then
			local team = GetAllianceIdx(worm.TeamIndex) + 1
			if NumOfWorms[team]==0 then
				NumOfTeams = NumOfTeams + 1
			end
			NumOfWorms[team] = NumOfWorms[team] + 1
			TotalHealth = TotalHealth + worm.Energy
		end
	end
	
	-- Redistribute total health to each team
	for i=1,4 do
		if NumOfWorms[i]>0 then
			TotalTeamHealth[i] = math.ceil(TotalHealth/NumOfTeams)
			TotalHealth = TotalHealth - TotalTeamHealth[i]
			NumOfTeams = NumOfTeams - 1
		end
	end
	
	-- Redistribute total team health to each worm
	for _,WormContainerName in GetAllWorms() do
		local lock, worm = EditContainer(WormContainerName)
		
		if worm.Active and worm.Energy>0 then
			local team = GetAllianceIdx(worm.TeamIndex) + 1
			
			-- This will redistribute health without any loss due to rounding issues
			local TargetHealth = math.ceil(TotalTeamHealth[team]/NumOfWorms[team])
			TotalTeamHealth[team] = TotalTeamHealth[team] - TargetHealth
			NumOfWorms[team] = NumOfWorms[team] - 1
			
			if worm.Energy ~= TargetHealth then
				-- Apply health using DamagePending, so the player can see how much health each worm gained/lost
				-- This will also kill worms whose health have been set to 0
				HealthChanged = true
				worm.DamagePending = worm.Energy - TargetHealth
			end
		end
		
		CloseContainer(lock)
	end
	if HealthChanged then SendMessage("GameLogic.ApplyDamage") end
end

------------------------------------------------------------------------

function SetWeaponCritical(ContainerName, team, critdata)
	if team<1 or team>4 then return end
	
	-- First weapon set as critical, let's initialize the critical trail so its colour matches the current team's colour
	if not CriticalWeaponData then
		CriticalWeaponData = {}
		
		local ParticleLock, Particle = EditContainer("WXPA_CritGlowTeam"..team)
		Particle.EmitterMaxParticles = 200
		Particle.EmitterSoundFX = "electricsparkout"
		CloseContainer(ParticleLock)
		ParticleLock, Particle = EditContainer("WXPA_CritTrailTeam"..team)
		Particle.EmitterMaxParticles = 200
		CloseContainer(ParticleLock)
	end
	
	local ContainerLock, Container = EditContainer(ContainerName)
	
	-- Store original weapon settings so they can be restored when the turn ends
	CriticalWeaponData[ContainerName] = {
		Container.WormDamageMagnitude,
		Container.WormDamageRadius,
		Container.LandDamageRadius,
		Container.ImpulseMagnitude,
		Container.ImpulseRadius,
		Container.ArielFx,
		Container.BombletWeaponName,
		Container.StopFxAtRest
	}
	
	-- Moar damage...
	Container.WormDamageMagnitude = Container.WormDamageMagnitude * 3
	Container.WormDamageRadius = Container.WormDamageRadius * 1.5
	Container.LandDamageRadius = Container.LandDamageRadius * 2
	Container.ImpulseMagnitude = Container.ImpulseMagnitude * 2
	Container.ImpulseRadius = Container.ImpulseRadius * 2
	-- ...and fancy glowing effects
	Container.ArielFx = critdata.Trail or "WXPA_CriticalGeneric"
	Container.StopFxAtRest = false
	
	-- obsolete
	if(critdata.SpecialCluster) then Container.BombletWeaponName = critdata.SpecialCluster..team end
	
	CloseContainer(ContainerLock)
	
	-- Apply the critical effect to all sub-weapons (clusters)
	for weapon,data in pairs(critdata.Related or {}) do
		SetWeaponCritical(weapon, team, data)
	end
end

function ResetWeaponCritical(ContainerName, critdata)
	if CriticalWeaponData and CriticalWeaponData[ContainerName] then
		-- Restore all original weapon settings
		local ContainerLock, Container = EditContainer(ContainerName)
		Container.WormDamageMagnitude = CriticalWeaponData[ContainerName][1]
		Container.WormDamageRadius = CriticalWeaponData[ContainerName][2]
		Container.LandDamageRadius = CriticalWeaponData[ContainerName][3]
		Container.ImpulseMagnitude = CriticalWeaponData[ContainerName][4]
		Container.ImpulseRadius = CriticalWeaponData[ContainerName][5]
		Container.ArielFx = CriticalWeaponData[ContainerName][6]
		Container.BombletWeaponName = CriticalWeaponData[ContainerName][7]
		Container.StopFxAtRest = CriticalWeaponData[ContainerName][8]
		CloseContainer(ContainerLock)
		
		for weapon,data in pairs(critdata.Related or {}) do
			ResetWeaponCritical(weapon, data)
		end
	end
end

function ResetCriticals()
	for weapon,data in pairs(WeaponCriticalChance) do
		ResetWeaponCritical(weapon, data)
	end
	for i=1,4 do
		local ParticleLock, Particle = EditContainer("WXPA_CritGlowTeam"..i)
		Particle.EmitterMaxParticles = 0
		Particle.EmitterSoundFX = ""
		CloseContainer(ParticleLock)
		ParticleLock, Particle = EditContainer("WXPA_CritTrailTeam"..i)
		Particle.EmitterMaxParticles = 0
		CloseContainer(ParticleLock)
	end
	
	CriticalWeaponData = nil
end
