# Tweaks

Each tweak must be compatible with _Kilburn's [W4ModLauncher](../../tools/W4ModLauncher).

## List of tweaks

- [ChaosW4](./ChaosW4) by _Kilburn ([_original post_](https://w4-tweaking.forumgaming.fr/t666-programme-w4modlauncher-nouveau-systeme-de-tweak))
- [CHTweak](./CHTweak) by CH ([_original post_](https://w4-tweaking.forumgaming.fr/t619-chtweak-1-4))
- [HTweak](./HTweak) by Hotman ([_original post_](https://w4-tweaking.forumgaming.fr/t1257-htweak))
- [S.T.P.](./S.T.P) by Tweak-Man and DamageMagnitude ([_original post_](https://w4-tweaking.forumgaming.fr/t1363-s-t-p-v-1-2))
- [T.T.M.D.M](./T.T.M.D.M). by Tweak-Man and DamageMagnitude ([_original post_](https://w4-tweaking.forumgaming.fr/t1218-t-t-m-d-m-v-1-5))
- [Worms4Havoc](./Worms4Havoc) by _Kilburn ([_original post_](https://w4-tweaking.forumgaming.fr/t1220-worms-4-havoc-version-inachevee))
- [WorksTweak](./WormsTweak) by _Kilburn ([_original post_](https://w4-tweaking.forumgaming.fr/t377-kilburn-wormstweakpatch100))
